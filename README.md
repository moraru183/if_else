
current_year = 2019

x=int(input('Which is your birth year? '))

if x < 1900 :
    print('Enter the corect year')
    exit()
elif x >= 2020 :
    print('Enter the corect year')
    exit()

age = current_year - x

print(f'You have {age} years')

if age <= 3 :
    print ('1-3 age - BABY')
elif age >= 4 and age <= 9:
    print ('4-9 age - KID')
elif age >= 10 and age <= 15:
    print ('10-15 age - TEEN')
elif age >= 16 and age <= 18:
    print ('16-18 age - YOUNG')
elif age >= 19 and age <= 50:
    print ('19-50 age - ADULT')
elif age >= 51 :
    print ('51 + age - OLD')
